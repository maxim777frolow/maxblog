from django.db import models
from django.utils.translation import ugettext_lazy as _

class PostAbstractModel(models.Model):
    created_datetime = models.DateTimeField(_('Creating time'), auto_now_add=True)
    update_datetime = models.DateTimeField(_('Last changing time'), auto_now_add=True)
    author = models.ForeignKey('accounts.User', on_delete=models.CASCADE, verbose_name=_('Author'))

    class Meta:
        abstract = True



class Post(PostAbstractModel):
    title = models.CharField(_("Title"), max_length=50)
    second_title = models.CharField(_("Second Title"), max_length=200)
    text = models.TextField(_("News text"))
    category = models.CharField(_("Category"), max_length=20)
    image = models.ImageField(_("News image"), upload_to='news_images/', null=True, blank=False)

    def __str__(self):
        return " ".join([str(self.created_datetime), self.title])

    class Meta:
        ordering = ('-created_datetime',)
        verbose_name = _("News")
        verbose_name_plural = _("News")

